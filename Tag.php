<?php


class Tag {
    /**
     * @var array
     */
    private $attrs;
    /**
     * @var string
     */
    private $text;
    /**
     * @var string
     */
    private $tagName;

    /**
     * @param string $tagName
     */
    public function __construct(string $tagName)
    {
        $this->tagName = $tagName;
    }

    /**
     * @param string $tagString
     * @return Tag
     */
    public static function instance(string $tagString): Tag
    {
        $html = new SimpleXMLElement($tagString);

        $tag = new Tag($html->getName());

        $tag->setText((string)$html);

        foreach ($html->attributes() as $key => $value) {
            $tag->setAttr($key, $value);
        }

        return $tag;
    }

    /**
     * @param string $text
     * @return $this
     */
    public function setText(string $text): Tag
    {
        $this->text = $text;
        return $this;
    }

    /**
     * @param string $attrName
     * @param string $attrValue
     * @return $this
     */
    public function setAttr(string $attrName, string $attrValue): Tag
    {
        $this->attrs[$attrName] = $attrValue;
        return $this;
    }

    /**
     * @return string
     */
    public function show(): string
    {
        $attrString = $this->attrString("%s = \"%s\"", ' ');

        return "<{$this->tagName} $attrString>$this->text</$this->tagName>";
    }

    /**
     * @param bool $withValues
     * @return string
     */
    public function attributes(bool $withValues = false): string
    {
        if ($withValues) {
            return $this->attrString("%s => %s", ", ");
        }

        return implode(',', array_keys($this->attrs));
    }

    /**
     * @param string $template
     * @param string $glue
     * @return string
     */
    private function attrString(string $template, string $glue): string
    {
        $attrs = [];
        foreach ($this->attrs as $key => $value) {
            $attrs[] = sprintf($template, $key, $value);
        }

        return implode($glue, $attrs);
    }
}