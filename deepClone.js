function deepClone(obj) {
    const visited = [];
    const cloned = [];

    const clone = item => {
        const isNotSet = (where, what) => where.indexOf(what) === -1;
        const isObject = item => typeof item === "object" && !Array.isArray(item);
        const isArray  = item => typeof item === "object" && Array.isArray(item);

        switch (true) {
            case isObject(item):
                if (isNotSet(visited, item)) {
                    const cloneObject = {};

                    visited.push(item);
                    cloned.push(cloneObject);

                    for (let i in item) {
                        if (item.hasOwnProperty(i)) {
                            cloneObject[i] = clone(item[i]);
                        }
                    }

                    return cloneObject;
                } else {
                    return cloned[visited.indexOf(item)];
                }
            case isArray(item):
                if (isNotSet(visited, item)) {
                    const cloneArray = [];
                    visited.push(item);
                    cloned.push(cloneArray);

                    for (let i of item) {
                        cloneArray.push(clone(item[i]));
                    }

                    return cloneArray;
                } else {
                    return cloned[visited.indexOf(item)];
                }
            default:
                return item;
        }
    }

    return clone(obj);
}

const data = {
    payload: {
        a: 1,
        b: 2
    },
    child: null
};

data.child = {
    payload: {
        a: 3,
        b: 4
    },
    child: null
};

data.child.child = {
    payload: {
        a: 1,
        b: 4
    },
    child: data
};

const clone = deepClone(data);
console.log(data);
console.log(clone);
